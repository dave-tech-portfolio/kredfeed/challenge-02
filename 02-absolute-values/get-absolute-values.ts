export default function getAbsoluteValues(array: number[]) {
    return array.map(x => Math.abs(x));
}