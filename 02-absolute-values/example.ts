import getAbsoluteValues from "./get-absolute-values";

export default function exampleAbsolute() {
    const array = [0, -9, 5, -21, 1, -1];

    console.log('Input: ', array);
    const result = getAbsoluteValues(array);
    console.log('Output: ', result);
}