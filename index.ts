import exampleRotate from "./01-left-rotation/example";
import exampleAbsolute from "./02-absolute-values/example";
import exampleRepeat from "./03-repeated-values/example";
import exampleAPI from "./04-api-simulator";


console.log('Example rotate');
exampleRotate();

console.log('\nExample absolute');
exampleAbsolute();

console.log('\nExample repeat');
exampleRepeat();

console.log('\nExample API');
exampleAPI();