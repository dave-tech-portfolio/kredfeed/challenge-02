export default function rotateLeft(shifts: number, array: number[]) {
    if (shifts > array.length) {
        throw new Error('Array is smaller');
    }

    if (shifts < 0) {
        throw new Error('Negative numbers are not allowed');
    }

    if (array.length < 1 || array.length == shifts) {
        return array;
    }

    const lastElements = array.splice(0, shifts);
    array.push(...lastElements);
    return array;
}
