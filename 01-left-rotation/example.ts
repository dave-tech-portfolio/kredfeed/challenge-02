import rotateLeft from "./rotate-left";

export default function exampleRotate() {
    const shifts = 3;
    const array = [3, 5, 6, 1, 9];

    console.log('Input: ', array);
    const result = rotateLeft(shifts, array);
    console.log('Output: ', result);
}