import repeatValues from "./repeat-values";

export default function exampleRepeat() {
    const repeat = 4;
    const array = [0, -9, 5, 21];

    console.log('Input: ', array);
    const result = repeatValues(repeat, array);
    console.log('Output: ', result);
}