export default function repeatValues(repeat: number, array: number[]) {
    if (repeat < 0) {
        throw new Error('Negative numbers are not allowed')
    }

    return array.map(value => Array(repeat).fill(value))
        .flat()
        .reverse();
}