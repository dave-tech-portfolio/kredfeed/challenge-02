import { formatUsers, getUsersAPI, sortUsers } from "./utils";


export default async function exampleAPI() {
    const limit = 10;
    const skip = 2;

    const users = await getUsersAPI(limit, skip);

    const sortedUsers = sortUsers(users);
    const formattedUsers = formatUsers(sortedUsers);

    console.log(formattedUsers);
}
