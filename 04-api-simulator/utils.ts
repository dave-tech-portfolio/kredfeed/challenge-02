import axios from 'axios';

export async function getUsersAPI(limit: number, skip: number): Promise<any> {
    const response = await axios.get(`https://randomuser.me/api/?results=${limit}`);
    return response.data.results;
}

export function sortUsers(users: any[]) {
    return users.sort((a, b) => {
        if (a.email < b.email) return -1;
        if (a.email > b.email) return 1;
        return 0;
    });
}

export function formatUsers(users: any[]) {
    return users.map(user => ({
        email: user.email,
        firstName: user.name.first,
        lastName: user.name.last,
        age: user.dob.age,
        city: user.location.city,
        phone: user.phone
    }));
}