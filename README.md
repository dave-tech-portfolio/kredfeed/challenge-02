I.- Setup

You can deownload dependencies using:

npm i

For running the project you can use:

npm run start

II.- Challenges.

    01.- Left Rotation

To solve this challenge, I used the native javascript method called "splice" to remove elements from the start of the array and add them to the end. This is efficient because it does not create a new array (although this may not be ideal in a pure function architecture), and we are also using native methods which are faster and the code is easy to understand.

    02.- Absolute Values

To complete this challenge, I used the native javascript function "map" to go through the array and calculate the absolute values without changing the original array. While this method creates a new array, the code is simple and easy to understand.

    03.- Repeated Values

For this challenge, I chose to use the native javascript methods "map" and "fill" to go through the array and create new values. I also used the "flat" function to organize the values into a one-dimensional array. It's important to note that the "flat" method is a function that has been supported since ECMAScript 2019.

    04.- API Simulation

In this challenge, I built functions to retrieve users from the API by passing a limit parameter, which allows me to get multiple users at a time. I also created a function to sort the users that were downloaded and another function to format the data using the "map" method to go through the list of users.